<?php
/**
 * @file
 * Context reaction class for Tealium data object variable injection.
 */

/**
 * Expose Tealium data object variable injection as a context reaction.
 */
class tealium_context_reaction_add_var extends context_reaction {
  /**
   * Allow users to specify which key/value pairs to add.
   */
  function options_form($context) {
    // Get existing values for this form.
    $values = $this->fetch_from_context($context);

    // Wrapper for key/value pairs.
    $form['tealium_vars'] = array(
      '#tree' => TRUE,
      '#prefix' => '<div id="tealium-kv-reaction">',
      '#suffix' => '</div>',
      '#theme' => 'tealium_kv_table',
    );

    // Add a blank key value item.
    $values['tealium_vars'][] = array();

    // Create the forms tealium tag line items.
    foreach ($values['tealium_vars'] as $value) {
      $form['tealium_vars'][] = $this->value_pair_form_item($value);
    }

    // Ajax button for adding a new row with key values to the table.
    $form['add_another'] = array(
      '#type' => 'button',
      '#value' => t('Add another'),
      '#ajax' => array(
        'callback' => 'tealium_context_kv_ajax_add',
        'wrapper' => 'tealium-kv-reaction',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );

    // Display the list of available placeholders if token module is installed.
    if (module_exists('token')) {
      $form['token_help'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('node', 'menu', 'term', 'user'),
      );
    }

    return $form;
  }

  /**
   * Submit handler for options form.
   * Remove any values marked for deletion and key values array.
   *
   * @param $values Submitted values from the options form.
   *
   * @return array Key value pairs.
   */
  function options_form_submit($values) {

    foreach ($values['tealium_vars'] as $delta => &$var) {
      // Remove any values marked for deletion.
      if ($var['delete'] == 1 || empty($var['key'])) {
        unset($values['tealium_vars'][$delta]);
      }
      // Cleanup up array to only include key and value pairs.
      else {
        unset($var['delete']);
      }
    }

    // Remove the add another button from the settings.
    unset($values['add_another']);

    return $values;
  }

  /**
   * Creates a line item on a value pair form.
   *
   * @param array $values Tealium tags entries with key/value pairs.
   */
  function value_pair_form_item($values = array()) {
    $value_pair_form_item = array(
      '#tree' => TRUE,
    );
    $value_pair_form_item['key'] = array(
      '#type' => 'textfield',
      '#title' => t('Key'),
      '#title_display' => 'invisible',
      '#default_value' => isset($values['key']) ? $values['key'] : '',
    );
    $value_pair_form_item['value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#title_display' => 'invisible',
      '#default_value' => isset($values['value']) ? $values['value'] : '',
    );
    $value_pair_form_item['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete'),
      '#title_display' => 'invisible',
      '#default_value' => 0,
    );

    return $value_pair_form_item;
  }

  /**
   * Return a data array keyed to the key values set in the value pair form.
   */
  function execute() {

    $tags = array();

    foreach ($this->get_contexts() as $context_name => $context) {
      if (!empty($context->reactions['tealium_add_var'])) {
        foreach ($context->reactions['tealium_add_var']['tealium_vars'] as $tag) {
          $tags[$tag['key']] = $tag['value'];
        }
      }
    }

    return $tags;
  }
}
