Tealium context integration
===========================
The Tealium context integration module enables site administrators to add 
custom Tealium universal data object tags to sites using tokens and the 
context module without having to use hooks or code.


What is Tealium?
----------------
Tealium is a 'tag management' solution. More information can be found on the
[Tealium project][tealium-project] page.


Installation
------------
Enable the module (context is a dependency). If you'd like to use token 
variables (eg 'Current node type' & 'Current taxonomy name') download and 
enable the Token module.


Using the UI
------------
Ensure that context_ui is enabled in order to configure Tealium reactions from 
using the Drupal administrative UI.


**Example 1:**
Define Tealium variables for every page on Drupal

1. Add a new context on admin/structure/context.
2. Under "Conditions" select "Sitewide context".
3. Tick "Always active" in the Sitewide context options form.
3. Select "Tealium variables" from the "Reactions" menu.
4. Enter key value pairs to be present in the utag_data array and sent to
   Tealium. You can use tokens within the 'Value' input box as provided below
   the form.
5. Save the context.

_Note: Any Key-Value pairs which have blank keys will be when the form is saved._


**Example 2:**
Override custom Tealium variables for a specific node page

1. Add a new context on admin/structure/context.
2. Under "Conditions" select "Path" and enter the path of the node (eg node/1).
3. Select "Tealium variables" from the "Reactions" menu.
4. Enter key value pairs to be present in the utag_data array and sent to
   Tealium. You can use tokens within the 'Value' input box as provided below
   the form.
5. Save the context.

_Note: Any Key-Value pairs which have blank keys will be when the form is saved._


Maintainers
-----------

- [interactivejunky][interactivejunkie] (Marton Bodonyi)


Contributors
------------

- [chOP][chop] (Christopher Hopper)


[tealium-project]: https://drupal.org/project/tealium
[interactivejunkie]: https://drupal.org/user/1633774
[chop]: https://drupal.org/user/116649
